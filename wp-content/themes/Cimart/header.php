<?php
/**
 * Created by PhpStorm.
 * User: jadmin
 * Date: 19/02/2018
 * Time: 11:30
 */
?>
<!doctype html>
<html lang="en">
<head>
    <title>
        <?php bloginfo('name'); ?>
        <?php if (is_home() || is_front_page()) : ?>
            <?php bloginfo('description'); ?>
        <?php else : ?>
            <?php wp_title("", true); ?>
        <?php endif; ?>
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri()?>/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="./js/lightbox-plus-jquery.js"></script>
    <?php wp_head();  ?>
</head>
<body >
<div class="container-fluid"  style="background-image: url('<?php echo get_template_directory_uri() . "/images/Fotolia_darken.jpg"; ?>')">

