<?php
/**
 * Created by PhpStorm.
 * User: jadmin
 * Date: 19/02/2018
 * Time: 11:31
 */


get_header();

?>
    <div class="row">
        <div class="page-logo">
            CIMART
        </div>
    </div>
    <div class="row page-content" >
        <div class="col-12 col-md-7 left-content">
            <h1><?php echo do_shortcode("[cgv tagline]"); ?></h1>
            <ul class="informations">
                <li><i class="fas fa-map-pin"></i> <?php echo do_shortcode("[cgv address]"); ?></li>
                <li><i class="fas fa-address-card"></i> <?php echo do_shortcode("[cgv email]"); ?></li>
                <li><i class="fas fa-phone"></i> <?php echo do_shortcode("[cgv phone]"); ?></li>
            </ul>
        </div>
        <div class="col-12 col-md-5 right-content">
            <?= do_shortcode("[contact-form-7 title=\"Contact form 1\"]")?>
        </div>
    </div>
<?php get_footer() ?>